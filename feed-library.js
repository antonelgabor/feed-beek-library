
/*
    How does this library work:
    First of all I send the request to get the survey (getQuestions), considerign cookies to avoid showing the same survey multiple times to the same user.
    If the response of the request is empty, it means that this user answered all the surveys; so the pop up won't be set up.
    On send button press the sendAnswer function get called and, basing on survey type, it will set up the input data for the request and send it.
    setCookie and getCookie are cookies management functions; fade purpose is to have the fading effect when the pop-up is set or have to disappear.
*/

/**
 * Function list:
    * startLibrary()
    * getQuestions()
    * setUpLibrary()
    * cookiesInfo()
    * sendAnswer()
 *
 * Service Functions:
    * fade()
    * setCookie()
    * getCookie()
 * 
 * Function flow:
 * startLibrary() -> getQuestions() -> setUpLibrary() -> cookiesInfo() -> sendAnswer()
 */

const apiKey = document.currentScript.getAttribute('apiKey');
localStorage.setItem('apiKey', apiKey);
var survey = "";

function startLibrary(apiKey = "") {
    if (apiKey == "") {
        window.alert("You forgot to insert the apiKey!")
    } else {
        localStorage.setItem('apiKey', apiKey);
        var before = getCookie('lastAnsw');
        before = new Date(before)
        var now = new Date()
        // Run the pop-up only if the last answer was sent at least five minutes ago
        if (now.getTime() - before.getTime() > 300000) {
            if (document.getElementById('feed-pop-up')) {
                window.onload = document.getElementById('feed-pop-up').innerHTML = '<div id="feed-content"><div id="feed-close-icon" class="feed-icon" ><span id="feed-x" title="Chiudi">x</span></div><div class="feed-header"><p id="feed-question"></p></div><div id="feed-answer" class="body"></div><div id="feed-footer" class="feed-footer"></div></div>'
                window.onload = getQuestions(apiKey);
                // Add listener for the closure button
                window.addEventListener("DOMContentLoaded", () => {
                    document.getElementById('feed-x').onclick = function () {
                        // Fadeout effect
                        setTimeout(function () {
                            fade('feed-pop-up', 'out', '1');
                        }, 100)
                        // Removal of the element
                        setTimeout(function () {
                            document.getElementById('feed-pop-up').innerHTML = '';
                        }, 1000)
                    }
                })
            }
        }
    }
}

startLibrary(apiKey);

// HOWTO SET STYLE FROM STRING
// var style = document.createElement('style'); z
// style.innerHTML = '#feed-question{color:green !important;}'
// document.head.appendChild(style)

// First api request, get survey
function getQuestions() {
    let apiKey = localStorage.getItem('apiKey');
    var api = "https://k1f5e4q00c.execute-api.eu-central-1.amazonaws.com/test/survey_lib";
    var req = new XMLHttpRequest();
    req.open("GET", api);

    var cookie = getCookie('feed-ids');

    req.setRequestHeader('app', JSON.stringify({ appId: apiKey, surveys: cookie ? JSON.parse(cookie) : [] }));

    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            survey = JSON.parse(req.responseText);
            localStorage.setItem("survey", JSON.stringify(survey));
        }
    }
    req.send();
    req.addEventListener('load', setUpLibrary, false)

}

// Start setting the pop-up after the request is completed
function setUpLibrary() {
    survey = JSON.parse(localStorage.getItem('survey'));

    if (!(Object.keys(survey).length === 0 && survey.constructor === Object)) {
        // Set the personal pop-up style

        var a = JSON.parse(survey.appStyle);

        // Pop up props
        var appStyle =
            "#feed-content{" +
            "font-family:'" + (a.ex_font ? a.ex_font : 'Arial') + "' !important;" +
            "border-radius: " + (a.ex_borderRadius ? a.ex_borderRadius + 'px' : '5px') + " !important;" +
            "background-color: " + (a.ex_backColor ? '#' + a.ex_backColor : 'white') + " !important;" +
            "border: " + (a.ex_borderColor ? 'solid 2px #' + a.ex_borderColor : 'none') + " !important;" +
            "font-size: " + (a.ex_size ? a.ex_size + 'px' : '16px') + " !important;" +
            "color: " + (a.ex_color ? a.ex_color : 'white') + " !important;" +
            "}";
        // Open answer input props
        appStyle +=
            "#feed-input{" +
            "border-radius: " + (a.ex_inputBorderRadius ? a.ex_inputBorderRadius + "px" : '5px') + ' !important;' +
            "background-color: " + (a.ex_inputBackColor ? '#' + a.ex_inputBackColor : 'white') + ' !important;' +
            "border: " + (a.ex_borderColor ? 'solid 2px #' + a.ex_borderColor : 'none') + ' !important;' +
            "color: " + (a.ex_inputTextColor ? '#' + a.ex_inputTextColor : 'black') + ' !important;' +
            "}";
        // Multiple choice style
        appStyle +=
            ".feed-checkbox-container input:checked ~ .feed-checkbox-checkmark{" +
            "background-color: " + (a.ex_multChColor1 ? '#' + a.ex_multChColor1 : 'white') + " !important;" +
            "}" +
            ".feed-checkbox-checkmark{" +
            "background-color: " + (a.ex_multChColor2 ? '#' + a.ex_multChColor2 : 'black') + " !important;" +
            "}";

        // Grades style
        appStyle +=
            ".feed-radio-container input:checked ~ .feed-radio-checkmark{" +
            "background-color: " + (a.ex_gradeColor1 ? '#' + a.ex_gradeColor1 : 'white') + " !important;" +
            "}" +
            ".feed-radio-checkmark{" +
            "background-color: " + (a.ex_gradeColor2 ? '#' + a.ex_gradeColor2 : 'white') + " !important;" +
            "}";

        // Button style
        appStyle +=
            ".feed-button{" +
            "background-color: " + (a.ex_buttonColor ? '#' + a.ex_buttonColor : "red") + " !important;" +
            "color: " + (a.ex_buttonTextColor ? '#' + a.ex_buttonTextColor : 'white') + ' !important;' +
            "}"

        var style = document.createElement('style');
        style.innerHTML = appStyle
        document.head.appendChild(style)
        cookiesInfo();
        setTimeout(function () {
            fade('feed-pop-up', 'in', '1');
        }, 100)
    } else {
        document.getElementById('feed-pop-up').innerHTML = '';
    }
}

// Inform the user about cookies usage

function cookiesInfo() {
    // Cookies information
    var questDiv = document.getElementById('feed-question');
    questDiv.innerHTML = "Questo pop-up utilizza dei cookie che non registrano alcun dato personale ma solo se l'utente ha compilato il questionario.";

    // End cookies information
    var footer = document.getElementById('feed-footer');
    footer.innerHTML = '<div id="feed-IGotIt" class="feed-button"> Ho capito </div>';
    document.getElementById('feed-IGotIt').onclick = function () {
        setPopUp()
    }
}

/**
 * Switch case to manage the html of the different questionary types
 */

function setPopUp() {

    // Question section

    var questDiv = document.getElementById('feed-question');
    questDiv.innerHTML = survey.text1;

    // Answer section
    var answDiv = document.getElementById('feed-answer');

    switch (survey.type) {
        case 'Voto':
            for (i = 1; i <= 5; i++) {
                answDiv.innerHTML += '<div class="feed-radio-box"><label class="feed-radio-container">' + i + '<input name="feed-radio" type="radio" value=' + i + ' id="feed-radio' + i + '"><span class="feed-radio-checkmark"></span></label></div>';
            }
            break;
        case 'Risposta aperta':
            answDiv.innerHTML = '<input id="feed-input" class="feed-input" type="text">'
            break;
        case 'Crocette':
            var options = JSON.parse(survey.options);
            var index = 1;
            options.forEach(element => {
                answDiv.innerHTML += '<label class="feed-checkbox-container"> ' + element + ' <input id="feed-checkbox' + index + '" type="checkbox"><span class="feed-checkbox-checkmark"></span></label>';
                index++;
            });
            break;
    }

    // Send button 
    var footer = document.getElementById('feed-footer');
    footer.innerHTML = '<div id="feed-send-button" class="feed-button"> Invia </div>';
    document.getElementById('feed-send-button').onclick = function () {
        sendAnswer();
    }
}

function sendAnswer() {
    let apiKey = localStorage.getItem('apiKey')

    var cookie = getCookie('feed-ids');
    var answer = "";
    // Switch case to set answer properly
    switch (survey.type) {
        case 'Voto':
            for (k = 1; k <= 5; k++) {
                if (document.getElementById('feed-radio' + k).checked) {
                    answer = document.getElementById('feed-radio' + k).value;
                }
            }
            break;
        case 'Risposta aperta':
            answer = document.getElementById("feed-input").value;
            break;
        case 'Crocette':
            answer = [];
            var options = JSON.parse(survey.options);
            index = 1;
            options.forEach(element => {
                if (document.getElementById('feed-checkbox' + index).checked) {
                    answer.push(index);
                }
                index++;
            });
            break;
    }

    // End switch case

    // SET REQUEST
    if (!(answer === "")) {
        var body = JSON.stringify({ 'id': survey.id, 'answer': answer, date: new Date().toJSON().slice(0, 10).replace(/-/g, '/') });
        var api = "https://46swwnoj6i.execute-api.eu-central-1.amazonaws.com/test/answer"
        var req = new XMLHttpRequest();

        req.open("POST", api);
        req.setRequestHeader('app', apiKey);

        // If req successful set the cookie
        req.onload = function () {
            if (req.status == 200) {
                // Update the cookie
                if (cookie) {
                    var arr = JSON.parse(cookie);
                    arr.push(survey.id);
                    cookie = JSON.stringify(arr);
                } else {
                    var arr = [survey.id]
                    cookie = JSON.stringify(arr)
                }
                setCookie('feed-ids', cookie)
                var now = new Date()
                setCookie('lastAnsw', now);
            }
        };
        req.send(body);

        //Request sent    
    }

    // Thank the user
    document.getElementById('feed-content').innerHTML = "<p>Grazie!</p>";
    // Fadeout effect
    setTimeout(function () {
        fade('feed-pop-up', 'out', '2');
    }, 1000)
    // Removal of the element
    setTimeout(function () {
        document.getElementById('feed-pop-up').innerHTML = '';
    }, 2500)

}

// Service functions

function fade(id, inOut, time) {
    {
        var el = document.getElementById(id);
        switch (inOut) {
            case "in":
                el.style.opacity = 1;
                break;
            case "out":
                el.style.opacity = 0;
                break;
        }
        el.style.transition = "opacity " + time + "s";
        el.style.WebkitTransition = "opacity " + time + "s";
    }
}

/**
 * Cookies managment functions
 */

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

